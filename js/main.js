var apartmentsList = new Vue({
    el: '#apartments-list',
    data: {
        apartmentsItems: []
    },
    created: function () {
        var createdApartmentsList = this;
        this.getDataJson().then(function (data) {
            createdApartmentsList.apartmentsItems = data;
            createdApartmentsList.$forceUpdate()
        }, function () {
            alert('error get data')
        });
    },
    methods:{
        getDataJson: function() {
            return new Promise(function(resolve, reject){
                $.ajax({
                    method: 'GET',
                    url: 'data.json',
                    success: function (data) {
                        if(data.apartments != undefined) {
                            resolve(data.apartments);
                        } else {
                            reject();
                        }
                    },
                    error: function () {
                        reject();
                    },
                    dataType: 'json'
                })
            });
        },
        formatPrice: function(value) {
            return new Intl.NumberFormat('nl-NL').format(value);
        }
    }
});
